<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LensesSelling extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'lenses_sellings';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';
}

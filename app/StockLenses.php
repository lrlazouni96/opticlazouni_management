<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockLenses extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'stock_lenses';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = ['lens_id', 'color_id', 'ray', 'diameter', 'power'];
}

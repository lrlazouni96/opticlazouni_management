<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockGlasses extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'stock_glasses';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = ['glass_id', 'color_id', 'sph', 'cyl', 'axe'];
}

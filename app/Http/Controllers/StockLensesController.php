<?php

namespace App\Http\Controllers;

use App\StockLenses;
use Illuminate\Http\Request;

class StockLensesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StockLenses  $stockLenses
     * @return \Illuminate\Http\Response
     */
    public function show(StockLenses $stockLenses)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StockLenses  $stockLenses
     * @return \Illuminate\Http\Response
     */
    public function edit(StockLenses $stockLenses)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StockLenses  $stockLenses
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StockLenses $stockLenses)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StockLenses  $stockLenses
     * @return \Illuminate\Http\Response
     */
    public function destroy(StockLenses $stockLenses)
    {
        //
    }
}

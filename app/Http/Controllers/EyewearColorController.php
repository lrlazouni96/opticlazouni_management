<?php

namespace App\Http\Controllers;

use App\EyewearColor;
use Illuminate\Http\Request;

class EyewearColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EyewearColor  $eyewearColor
     * @return \Illuminate\Http\Response
     */
    public function show(EyewearColor $eyewearColor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EyewearColor  $eyewearColor
     * @return \Illuminate\Http\Response
     */
    public function edit(EyewearColor $eyewearColor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EyewearColor  $eyewearColor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EyewearColor $eyewearColor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EyewearColor  $eyewearColor
     * @return \Illuminate\Http\Response
     */
    public function destroy(EyewearColor $eyewearColor)
    {
        //
    }
}

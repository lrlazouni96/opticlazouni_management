<?php

namespace App\Http\Controllers;

use App\StockEyewear;
use Illuminate\Http\Request;

class StockEyewearController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StockEyewear  $stockEyewear
     * @return \Illuminate\Http\Response
     */
    public function show(StockEyewear $stockEyewear)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StockEyewear  $stockEyewear
     * @return \Illuminate\Http\Response
     */
    public function edit(StockEyewear $stockEyewear)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StockEyewear  $stockEyewear
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StockEyewear $stockEyewear)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StockEyewear  $stockEyewear
     * @return \Illuminate\Http\Response
     */
    public function destroy(StockEyewear $stockEyewear)
    {
        //
    }
}

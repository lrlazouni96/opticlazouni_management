<?php

namespace App\Http\Controllers;

use App\GlassColor;
use Illuminate\Http\Request;

class GlassColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GlassColor  $glassColor
     * @return \Illuminate\Http\Response
     */
    public function show(GlassColor $glassColor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GlassColor  $glassColor
     * @return \Illuminate\Http\Response
     */
    public function edit(GlassColor $glassColor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GlassColor  $glassColor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GlassColor $glassColor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GlassColor  $glassColor
     * @return \Illuminate\Http\Response
     */
    public function destroy(GlassColor $glassColor)
    {
        //
    }
}

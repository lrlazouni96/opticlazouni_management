<?php

namespace App\Http\Controllers;

use App\StockGlasses;
use Illuminate\Http\Request;

class StockGlassesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StockGlasses  $stockGlasses
     * @return \Illuminate\Http\Response
     */
    public function show(StockGlasses $stockGlasses)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StockGlasses  $stockGlasses
     * @return \Illuminate\Http\Response
     */
    public function edit(StockGlasses $stockGlasses)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StockGlasses  $stockGlasses
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StockGlasses $stockGlasses)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StockGlasses  $stockGlasses
     * @return \Illuminate\Http\Response
     */
    public function destroy(StockGlasses $stockGlasses)
    {
        //
    }
}

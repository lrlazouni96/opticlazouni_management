<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clients';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    public function eyewears()
    {
        return $this->belongsToMany('App\Eyewear');
    }

    public function lenses()
    {
        return $this->belongsToMany('App\Lens');
    }

    public function montages()
    {
        return $this->hasMany('App\Montage');
    }

    public function glasses()
    {
        return $this->belongsToMany('App\Glass');
    }
}

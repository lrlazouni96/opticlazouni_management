<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockEyewear extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'stock_eyewears';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = ['eyewear_id', 'color_id','size'];
}

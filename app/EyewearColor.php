<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EyewearColor extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'eyewear_colors';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    public function eyewears()
    {
        return $this->belongsToMany('App\Eyewear');
    }
}

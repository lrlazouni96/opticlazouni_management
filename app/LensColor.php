<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LensColor extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'lens_colors';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    public function glasses()
    {
        return $this->belongsToMany('App\Glass');
    }
}

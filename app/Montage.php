<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Montage extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'montages';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    public function eyewear()
    {
        return $this->hasOne('App\Eyewear', 'stock_eyewear_id');
    }

    public function glass_od()
    {
        return $this->hasOne('App\Glass', 'stock_glass_id_od', 'id');
    }

    public function glass_os()
    {
        return $this->hasOne('App\Glass', 'stock_glass_id_os', 'id');
    }

    public function client()
    {
        return $this->belongsTo('App\Client');
    }
}

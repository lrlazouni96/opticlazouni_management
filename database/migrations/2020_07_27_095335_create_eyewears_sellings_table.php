<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEyewearsSellingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eyewears_sellings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('client_id');
            $table->foreignId('stock_eyewear_id');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('stock_eyewear_id')->references('eyewear_id')->on('stock_eyewears');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eyewears_sellings');
    }
}

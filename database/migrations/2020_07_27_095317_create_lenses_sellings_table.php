<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLensesSellingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lenses_sellings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('client_id');
            $table->foreignId('stock_lens_id');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('stock_lens_id')->references('lens_id')->on('stock_lenses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lenses_sellings');
    }
}

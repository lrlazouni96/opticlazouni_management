<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGlassesSellingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('glasses_sellings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('client_id');
            $table->foreignId('stock_glass_id');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('stock_glass_id')->references('glass_id')->on('stock_glasses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('glasses_sellings');
    }
}

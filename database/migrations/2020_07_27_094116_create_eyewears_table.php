<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEyewearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eyewears', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('purchasing_price');
            $table->integer('selling_price');
            $table->foreignId('brand_id');
            $table->foreign('brand_id')->references('id')->on('eyewears_brands');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eyewears');
    }
}
